#include <ros/ros.h>
#include <ros/console.h>
#include <filter_test/Header.h>
#include <fkie_message_filters/fkie_message_filters.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/synchronizer.h>

namespace fmf = fkie_message_filters;
namespace mf = message_filters;
using namespace filter_test;

int publish_frequency;
double time_difference;
int queue_size;

double max_timespan;

void mf_exact_callback(const HeaderConstPtr& msg0, const HeaderConstPtr& msg1)
{
    ROS_INFO("Default MF. Exact Time. Queue size: %d. Stamps: [%f : %f]. Seqs: [%d : %d]", 
        queue_size, msg0->header.stamp.toSec(), msg1->header.stamp.toSec(),
        msg0->header.seq, msg1->header.seq);
}

void mf_app_callback(const HeaderConstPtr& msg0, const HeaderConstPtr& msg1)
{
    ROS_INFO("Default MF. Approximate Time. Queue size: %d. Stamps: [%f : %f]. Seqs: [%d : %d]", 
        queue_size, msg0->header.stamp.toSec(), msg1->header.stamp.toSec(),
        msg0->header.seq, msg1->header.seq);
}

bool fmf_exact_callback(const ros::MessageEvent<const Header>& me0,
    const ros::MessageEvent<const Header>& me1)
{
    ROS_INFO("FKIE MF. Exact Time. Queue size: %d. Stamps: [%f : %f]. Seqs: [%d : %d]",
        queue_size, me0.getMessage()->header.stamp.toSec(),
        me1.getMessage()->header.stamp.toSec(),
        me0.getMessage()->header.seq,
        me1.getMessage()->header.seq);
    return true;
}

bool fmf_app_callback(const ros::MessageEvent<const Header>& me0,
    const ros::MessageEvent<const Header>& me1)
{
    ROS_INFO("FKIE MF. Approximate Time. Queue size: %d. Stamps: [%f : %f]. Seqs: [%d : %d]", 
        queue_size, me0.getMessage()->header.stamp.toSec(),
        me1.getMessage()->header.stamp.toSec(),
        me0.getMessage()->header.seq,
        me1.getMessage()->header.seq);
    return true;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "filter_test");
    ros::NodeHandle nh, nhp("~");

    if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
        ros::console::notifyLoggerLevelsChanged();
    }

    nhp.param("publish_frequency", publish_frequency, 10);
    nhp.param("time_difference", time_difference, 0.05);
    nhp.param("queue_size", queue_size, 10);

    nhp.param("max_timespan", max_timespan, 0.05);

    // Publishers for topic0 and topic1
    ros::Publisher pub0 = nh.advertise<Header>("topic0", 100);
    ros::Publisher pub1 = nh.advertise<Header>("topic1", 100);
    

    // Default message filters
    // Exact Time
    mf::Subscriber<Header> mf_exact_sub0(nh, "topic0", 1);
    mf::Subscriber<Header> mf_exact_sub1(nh, "topic1", 1);
    typedef mf::sync_policies::ExactTime<Header, Header> MfExactPolicy;
    mf::Synchronizer<MfExactPolicy> mf_exact_sync(MfExactPolicy(queue_size),
        mf_exact_sub0, mf_exact_sub1);
    mf_exact_sync.registerCallback(boost::bind(&mf_exact_callback, _1, _2));

    // Approximate Time
    mf::Subscriber<Header> mf_app_sub0(nh, "topic0", 1);
    mf::Subscriber<Header> mf_app_sub1(nh, "topic1", 1);
    typedef mf::sync_policies::ApproximateTime<Header, Header> MfApproximatePolicy;
    mf::Synchronizer<MfApproximatePolicy> mf_app_sync(MfApproximatePolicy(queue_size),
        mf_app_sub0, mf_app_sub1);
    mf_app_sync.registerCallback(boost::bind(&mf_app_callback, _1, _2));

    // FKIE message filters
    // Exact Time
    fmf::Subscriber<Header> fmf_exact_sub0(nh, "topic0", 1);
    fmf::Subscriber<Header> fmf_exact_sub1(nh, "topic1", 1);
    typedef fmf::Combiner<fmf::combiner_policies::ExactTime, fmf::Subscriber<Header>::Output,
        fmf::Subscriber<Header>::Output> CombinerExact;
    CombinerExact fmf_exact_comb;
    fmf::SimpleUserFilter<CombinerExact::Output> exact_sink;
    fmf_exact_comb.connect_to_sources(fmf_exact_sub0, fmf_exact_sub1);
    fmf_exact_comb.connect_to_sink(exact_sink);
    exact_sink.set_processing_function(fmf_exact_callback);

    // Approximate Time
    fmf::Subscriber<Header> fmf_app_sub0(nh, "topic0", 1);
    fmf::Subscriber<Header> fmf_app_sub1(nh, "topic1", 1);
    typedef fmf::Combiner<fmf::combiner_policies::ApproximateTime, fmf::Subscriber<Header>::Output,
        fmf::Subscriber<Header>::Output> CombinerApproximate;
    CombinerApproximate::Policy app_policy;
    app_policy.set_max_timespan(ros::Duration(max_timespan));
    CombinerApproximate fmf_app_comb(app_policy);
    fmf::SimpleUserFilter<CombinerApproximate::Output> app_sink;
    fmf_app_comb.connect_to_sources(fmf_app_sub0, fmf_app_sub1);
    fmf_app_comb.connect_to_sink(app_sink);
    app_sink.set_processing_function(fmf_app_callback);

    ros::Rate loop_rate(publish_frequency);
    ROS_WARN("Frequency: %d Hz. Time difference: %f s", publish_frequency, time_difference);
    while (ros::ok())
    {   
        std::cout << "\n";
        Header out;
        out.header.stamp = ros::Time::now();
        pub0.publish(out);
        out.header.stamp += ros::Duration(time_difference);
        pub1.publish(out);
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}